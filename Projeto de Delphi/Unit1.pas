unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB, Data.Win.ADODB, Vcl.Menus;

type
  Tprincipal = class(TForm)
    MainMenu1: TMainMenu;
    Estoque1: TMenuItem;
    Estoque2: TMenuItem;
    Funcionrios1: TMenuItem;
    Clientes1: TMenuItem;
    Sair1: TMenuItem;
    ADOConnection1: TADOConnection;
    CadastrodeProdutos1: TMenuItem;
    ADOTable1: TADOTable;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure Estoque2Click(Sender: TObject);
    procedure CadastrodeProdutos1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  principal: Tprincipal;

implementation

{$R *.dfm}

uses Unit2, Unit3;

procedure Tprincipal.CadastrodeProdutos1Click(Sender: TObject);
begin
cadProd := tcadProd.Create(self);
end;

procedure Tprincipal.Estoque2Click(Sender: TObject);
begin
estoque := testoque.Create(self);
end;

procedure Tprincipal.FormClose(Sender: TObject; var Action: TCloseAction);
begin
if MessageDlg('Encerrar Aplica��o',mtConfirmation,[mbOk,mbCancel],0)
= mrOk then
Action := caFree
else
Action := caNone;
end;

end.
