unit Unit3;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.Win.ADODB, Data.DB, Vcl.StdCtrls,
  Vcl.Buttons, Vcl.ExtCtrls, Vcl.DBCtrls, Vcl.Mask;

type
  TcadProd = class(TForm)
    Label1: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    DBEdit1: TDBEdit;
    DBComboBox1: TDBComboBox;
    DBRadioGroup1: TDBRadioGroup;
    DBEdit2: TDBEdit;
    DataSource1: TDataSource;
    ADOQuery1: TADOQuery;
    DBNavigator1: TDBNavigator;
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  cadProd: TcadProd;

implementation

{$R *.dfm}

uses Unit1;

procedure TcadProd.Button1Click(Sender: TObject);
begin
ADOQuery1.Post;
end;

procedure TcadProd.Button2Click(Sender: TObject);
begin
ADOQuery1.Append;
end;

end.
