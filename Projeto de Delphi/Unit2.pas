unit Unit2;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.Win.ADODB, Vcl.StdCtrls,
  Vcl.ExtCtrls, Vcl.DBCtrls, Vcl.Mask, Data.DB, Vcl.Grids, Vcl.DBGrids,
  Vcl.Buttons;

type
  Testoque = class(TForm)
    DBGrid1: TDBGrid;
    Label1: TLabel;
    Button1: TButton;
    Edit1: TEdit;
    ADOQuery1: TADOQuery;
    DataSource1: TDataSource;
    Label2: TLabel;
    Edit2: TEdit;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Edit3: TEdit;
    DBComboBox1: TDBComboBox;
    DBComboBox2: TDBComboBox;
    procedure Button1Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  estoque: Testoque;

implementation

{$R *.dfm}

uses Unit1;

procedure Testoque.Button1Click(Sender: TObject);
begin

   if (Edit1.Text <> '') or (Edit2.Text <> '') then
    Begin
     ADOQuery1.Active := False;
     ADOQuery1.SQL.Clear;
     ADOQuery1.SQL.Add('select * from estoquedelphi where nome like "' + Edit1.Text + '%"');
     ADOQuery1.Active := True;
    End
  else
  Begin
     ADOQuery1.Active := false;
     ADOQuery1.SQL.Clear;
     ADOQuery1.SQL.Add('select * from estoquedelphi');
     ADOQuery1.Active := True;
  End;
  end;
end.
